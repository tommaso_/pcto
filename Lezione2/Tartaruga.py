import ipyturtle3 as turtle
from ipyturtle3 import hold_canvas
from IPython import get_ipython

def riavvia():
    get_ipython().magic('reset -f') 

def fissa():
    get_ipython().run_cell_magic(u'HTML', u'', u'<style>canvas.lm-Widget.p-Widget{position: fixed; top: 60px; right: 40px; z-index: 10000;}</style>')
    print("Cancella l'output di questa cella per ripristinare la visualizzazione in linea!")
    
class Tartaruga:
    def __init__(self, w=500, h=550, bg="white"):
        self.myCanvas = turtle.Canvas(width=w, height=h)
        display(self.myCanvas)
        self.canvas_size=(w, h)
        self.myTS=turtle.TurtleScreen(self.myCanvas)        
        self.cancella(bg)  
    def sinistra(self, q):
        self.turtle.left(q)
    def destra(self, q):
        self.turtle.right(q)
    def avanti(self, q):
        self.turtle.forward(q)
    def indietro(self, q):
        self.turtle.backward(q)
    def solleva(self):
        self.turtle.up()
    def posa(self):
        self.turtle.down()
    def sposta(self, x, y):
        self.turtle.goto(x, y)    
    def colore(self, c):
        self.turtle.pencolor(c)
    def cancella(self, bg="white"):
        self.myTS.clear()
        self.myTS.bgcolor(bg)
        self.turtle = turtle.Turtle(self.myTS)
        self.turtle.shape("turtle")            
        self.casa()
    def casa(self):        
        self.turtle.home()        
    def sollevaCasaPosa(self):        
        self.solleva()
        self.turtle.home() 
        self.posa()
    def sollevaSpostaPosa(self, x, y):        
        self.solleva()
        self.sposta(x, y)
        self.posa()
    def cerchio(self, raggio, angolo=360):
        self.turtle.circle(raggio, angolo)
    def iniziaRiempi(self, colore):
        self.turtle.begin_fill()
        self.turtle.fillcolor(colore)
    def terminaRiempi(self):
        self.turtle.end_fill()
    def spessore(self, s):
        self.turtle.pensize(s)
    def velocità(self, valore):
        self.turtle.speed(valore)
    def posizione(self):
        return self.turtle.pos()
    def xy(self):        
        self.indietro(self.canvas_size[0]//2)
        self.sollevaCasaPosa()
        self.avanti(self.canvas_size[0]//2)        
        self.iniziaRiempi("black")
        self.sinistra(150)
        self.avanti(10)
        self.sinistra(120)
        self.avanti(10)
        self.sinistra(120)
        self.avanti(10)
        self.terminaRiempi()
        self.sollevaCasaPosa()
        self.sinistra(90)
        self.indietro(self.canvas_size[1]//2)        
        self.sollevaCasaPosa()
        self.sinistra(90)
        self.avanti(self.canvas_size[1]//2)
        self.iniziaRiempi("black")
        self.sinistra(150)
        self.avanti(10)
        self.sinistra(120)
        self.avanti(10)
        self.sinistra(120)
        self.avanti(10)
        self.terminaRiempi()
        self.sollevaCasaPosa()        
